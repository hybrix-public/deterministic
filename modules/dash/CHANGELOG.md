2024.03.07
- started changelog per coin module

2024.03.28
- created minimal library based on bitcore for Dash
- dashcore-lib in npm is buggy, so this older version is kept separate!
- keep it separate from other node modules!


