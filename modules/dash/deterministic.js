// (C) 2024 hybrix / Gijs-Jan van Dompseler / Joachim de Koning
// Deterministic encryption wrapper for Dash

const dashcore = require('./dashcore-lib/index.js')

/**
 * @param address
 * @param data
 */
const transformUtxo = data => unspent => ({
  address: data.source,
  outputIndex: unspent.txn,
  satoshis: Number(unspent.amount),
  script: unspent.script,
  txId: unspent.txid
});

let wrapper = (
  function () {
    let functions = {
      // create deterministic public and private keys based on a seed
      // https://github.com/dashevo/dashcore-lib/blob/master/docs/examples.md
      keys: function (data) {
        let seed = Buffer.from(data.seed);
        let hash = dashcore.crypto.Hash.sha256(seed);
        let bn = dashcore.crypto.BN.fromBuffer(hash);

        let privateKey = new dashcore.PrivateKey(bn, data.mode);
        let wif = privateKey.toWIF();

        return { WIF: wif };
      },
      // TODO importPublic
      // TODO sumKeys

      importPrivate: function (data) {
        return {WIF: data.privateKey};
      },

      // generate a unique wallet address from a given public key
      address: function (data) {
        let privateKey = new dashcore.PrivateKey(data.WIF, data.mode);
        let address = privateKey.toAddress();
        if (!dashcore.Address.isValid(address, data.mode)) {
          throw new Error("Can't generate address from private key. " +
                             'Generated address ' + address +
                             'is not valid for ' + data.mode);
        }

        return address.toString();
      },

      // return public key
      publickey: function (data) {
        let privKey = dashcore.PrivateKey(data.WIF, data.mode);
        return new dashcore.PublicKey(privKey).toString('hex');
      },

      // return private key
      privatekey: function (data) {
        return data.WIF;
      },

      transaction: function (data) {
        const privKey = dashcore.PrivateKey(data.keys.WIF, data.mode);
        //const recipientAddr = dashcore.Address(data.target, data.mode);
        const recipientAddr = data.target;
        //const changeAddr = dashcore.Address(data.source, data.mode);
        const changeAddr = data.source;
        const hasValidMessage = typeof data.message === 'string';
        const utxos = data.unspent.unspents.map(transformUtxo(data));          

        const tx = new dashcore.Transaction()
          /*
          .from(data.unspent.unspents.map(function (utxo) {
            return { txId: utxo.txid,
              outputIndex: utxo.txn,
              address: utxo.address,
              script: utxo.script,
              satoshis: parseInt(utxo.amount)
            };
          }))*/
          .from(utxos)
          .change(changeAddr)
          .fee(parseInt(data.fee))
          .to(recipientAddr, parseInt(data.amount));

        const txWithMaybeMessage = hasValidMessage
          ? tx.addData(data.message)
          : tx;

        const signedTx = txWithMaybeMessage
          .sign(privKey)
          .uncheckedSerialize();

        return signedTx
      }
    };

    return functions;
  }
)();

// export the functionality to a pre-prepared var
window.deterministic = wrapper;
