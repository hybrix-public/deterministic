'use strict';

let buffer = require('buffer');

let Signature = require('../crypto/signature');
let Script = require('../script');
let Output = require('./output');
let BufferReader = require('../encoding/bufferreader');
let BufferWriter = require('../encoding/bufferwriter');
let BN = require('../crypto/bn');
let Hash = require('../crypto/hash');
let ECDSA = require('../crypto/ecdsa');
let $ = require('../util/preconditions');
let BufferUtil = require('../util/buffer');
let blake2b = require('blake2b');
let _ = require('lodash');

let SIGHASH_SINGLE_BUG = '0000000000000000000000000000000000000000000000000000000000000001';
let BITS_64_ON = 'ffffffffffffffff';
let ZERO = Buffer.from('0000000000000000000000000000000000000000000000000000000000000000', 'hex');

let sighashSapling = function sighash (transaction, sighashType, inputNumber, subscript) {
  // Copyright (C) 2019 by LitecoinZ Developers.
  //
  // Permission is hereby granted, free of charge, to any person obtaining a copy
  // of this software and associated documentation files (the "Software"), to deal
  // in the Software without restriction, including without limitation the rights
  // to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  // copies of the Software, and to permit persons to whom the Software is
  // furnished to do so, subject to the following conditions:
  //
  // The above copyright notice and this permission notice shall be included in all
  // copies or substantial portions of the Software.
  //
  // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  // SOFTWARE.
  //
  let input = transaction.inputs[inputNumber];

  /**
   * @param bufferToHash
   * @param personalization
   */
  function getBlake2bHash (bufferToHash, personalization) {
    let out = Buffer.allocUnsafe(32);
    return blake2b(out.length, null, null, Buffer.from(personalization)).update(bufferToHash).digest(out);
  }

  /**
   * @param tx
   */
  function GetPrevoutHash (tx) {
    let writer = new BufferWriter();

    _.each(tx.inputs, function (input) {
      writer.writeReverse(input.prevTxId);
      writer.writeUInt32LE(input.outputIndex);
    });

    return getBlake2bHash(writer.toBuffer(), 'ZcashPrevoutHash');
  }

  /**
   * @param tx
   */
  function GetSequenceHash (tx) {
    let writer = new BufferWriter();

    _.each(tx.inputs, function (input) {
      writer.writeUInt32LE(input.sequenceNumber);
    });

    return getBlake2bHash(writer.toBuffer(), 'ZcashSequencHash');
  }

  /**
   * @param tx
   * @param n
   */
  function GetOutputsHash (tx, n) {
    let writer = new BufferWriter();

    if (_.isUndefined(n)) {
      _.each(tx.outputs, function (output) {
        output.toBufferWriter(writer);
      });
    } else {
      tx.outputs[n].toBufferWriter(writer);
    }

    return getBlake2bHash(writer.toBuffer(), 'ZcashOutputsHash');
  }

  let hashPrevouts = ZERO;
  let hashSequence = ZERO;
  let hashOutputs = ZERO;
  let hashJoinSplits = ZERO;
  let hashShieldedSpends = ZERO;
  let hashShieldedOutputs = ZERO;

  let writer = new BufferWriter();

  // header of the transaction (4-byte little endian)
  let header = transaction.version | (1 << 31);
  writer.writeInt32LE(header);

  // nVersionGroupId of the transaction (4-byte little endian)
  writer.writeUInt32LE(transaction.nVersionGroupId);

  // hashPrevouts (32-byte hash)
  if (!(sighashType & Signature.SIGHASH_ANYONECANPAY)) {
    hashPrevouts = GetPrevoutHash(transaction);
  }
  writer.write(hashPrevouts);

  // hashSequence (32-byte hash)
  if (!(sighashType & Signature.SIGHASH_ANYONECANPAY) &&
    (sighashType & 31) != Signature.SIGHASH_SINGLE &&
    (sighashType & 31) != Signature.SIGHASH_NONE) {
    hashSequence = GetSequenceHash(transaction);
  }
  writer.write(hashSequence);

  // hashOutputs (32-byte hash)
  if ((sighashType & 31) != Signature.SIGHASH_SINGLE && (sighashType & 31) != Signature.SIGHASH_NONE) {
    hashOutputs = GetOutputsHash(transaction);
  } else if ((sighashType & 31) == Signature.SIGHASH_SINGLE && inputNumber < transaction.outputs.length) {
    hashOutputs = GetOutputsHash(transaction, inputNumber);
  }
  writer.write(hashOutputs);

  // hashJoinSplits (32-byte hash)
  writer.write(hashJoinSplits);

  // hashShieldedSpends (32-byte hash)
  writer.write(hashShieldedSpends);

  // hashShieldedOutputs (32-byte hash)
  writer.write(hashShieldedOutputs);

  // nLockTime of the transaction (4-byte little endian)
  writer.writeUInt32LE(transaction.nLockTime);

  // nExpiryHeight of the transaction (4-byte little endian)
  writer.writeUInt32LE(transaction.nExpiryHeight);

  // valueBalance of the transaction (8-byte little endian)
  writer.writeUInt64LE(transaction.valueBalance);

  // sighash type of the signature (4-byte little endian)
  writer.writeUInt32LE(sighashType >>> 0);

  // outpoint (32-byte hash + 4-byte little endian)
  writer.writeReverse(input.prevTxId);
  writer.writeUInt32LE(input.outputIndex);

  // scriptCode of the input (serialized as scripts inside CTxOuts)
  writer.writeVarintNum(subscript.toBuffer().length);
  writer.write(subscript.toBuffer());

  // value of the output spent by this input (8-byte little endian)
  writer.writeUInt64LE(input.output.satoshis);

  // nSequence of the input (4-byte little endian)
  let sequenceNumber = input.sequenceNumber;
  writer.writeUInt32LE(sequenceNumber);

  let personalization = Buffer.alloc(16);
  let prefix = 'ZcashSigHash';
  let consensusBranchId = 3925833126; // hybrix: was 1991772603;

  personalization.write(prefix);
  personalization.writeUInt32LE(consensusBranchId, prefix.length);

  let ret = getBlake2bHash(writer.toBuffer(), personalization);
  ret = new BufferReader(ret).readReverse();
  return ret;
};

/**
 * Returns a buffer of length 32 bytes with the hash that needs to be signed
 * for OP_CHECKSIG.
 *
 * @name Signing.sighash
 * @param {Transaction} transaction The transaction to sign.
 * @param {number} sighashType The type of the hash.
 * @param {number} inputNumber The input index for the signature.
 * @param {Script} subscript The script that will be signed.
 */
let sighash = function sighash (transaction, sighashType, inputNumber, subscript) {
  let Transaction = require('./transaction');
  let Input = require('./input');

  if (transaction.version >= 4) {
    return sighashSapling(transaction, sighashType, inputNumber, subscript);
  }

  let i;
  // Copy transaction
  let txcopy = Transaction.shallowCopy(transaction);

  // Copy script
  subscript = new Script(subscript);
  subscript.removeCodeseparators();

  for (i = 0; i < txcopy.inputs.length; i++) {
    // Blank signatures for other inputs
    txcopy.inputs[i] = new Input(txcopy.inputs[i]).setScript(Script.empty());
  }

  txcopy.inputs[inputNumber] = new Input(txcopy.inputs[inputNumber]).setScript(subscript);

  if ((sighashType & 31) === Signature.SIGHASH_NONE ||
    (sighashType & 31) === Signature.SIGHASH_SINGLE) {
    // clear all sequenceNumbers
    for (i = 0; i < txcopy.inputs.length; i++) {
      if (i !== inputNumber) {
        txcopy.inputs[i].sequenceNumber = 0;
      }
    }
  }

  if ((sighashType & 31) === Signature.SIGHASH_NONE) {
    txcopy.outputs = [];
  } else if ((sighashType & 31) === Signature.SIGHASH_SINGLE) {
    // The SIGHASH_SINGLE bug.
    // https://bitcointalk.org/index.php?topic=260595.0
    if (inputNumber >= txcopy.outputs.length) {
      return Buffer.from(SIGHASH_SINGLE_BUG, 'hex');
    }

    txcopy.outputs.length = inputNumber + 1;

    for (i = 0; i < inputNumber; i++) {
      txcopy.outputs[i] = new Output({
        satoshis: BN.fromBuffer(new buffer.Buffer(BITS_64_ON, 'hex')),
        script: Script.empty()
      });
    }
  }

  if (sighashType & Signature.SIGHASH_ANYONECANPAY) {
    txcopy.inputs = [txcopy.inputs[inputNumber]];
  }

  let buf = new BufferWriter()
    .write(txcopy.toBuffer())
    .writeInt32LE(sighashType)
    .toBuffer();
  let ret = Hash.sha256sha256(buf);
  ret = new BufferReader(ret).readReverse();
  return ret;
};

/**
 * Create a signature.
 *
 * @name Signing.sign
 * @param {Transaction} transaction
 * @param {PrivateKey} privateKey
 * @param {number} sighash
 * @param sighashType
 * @param {number} inputIndex
 * @param {Script} subscript
 * @returns {Signature}
 */
function sign (transaction, privateKey, sighashType, inputIndex, subscript) {
  let hashbuf = sighash(transaction, sighashType, inputIndex, subscript);
  let sig = ECDSA.sign(hashbuf, privateKey, 'little').set({
    nhashtype: sighashType
  });
  return sig;
}

/**
 * Verify a signature.
 *
 * @name Signing.verify
 * @param {Transaction} transaction
 * @param {Signature} signature
 * @param {PublicKey} publicKey
 * @param {number} inputIndex
 * @param {Script} subscript
 * @returns {boolean}
 */
function verify (transaction, signature, publicKey, inputIndex, subscript) {
  $.checkArgument(!_.isUndefined(transaction));
  $.checkArgument(!_.isUndefined(signature) && !_.isUndefined(signature.nhashtype));
  let hashbuf = sighash(transaction, signature.nhashtype, inputIndex, subscript);
  return ECDSA.verify(hashbuf, signature, publicKey, 'little');
}

/**
 * @namespace Signing
 */
module.exports = {
  sighash: sighash,
  sign: sign,
  verify: verify
};
