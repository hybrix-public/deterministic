#!/bin/sh
OLDPATH=$PATH
WHEREAMI=`pwd`

# $HYBRIXD/deterministic/modules/zcash  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
DETERMINISTIC="$HYBRIXD/deterministic"
MODULE="$DETERMINISTIC/modules/zcash"

# Replace all zcash loaders with minimalist versions
replaceBitcoreLoader () {
  [ ! -f "$PPATH/index.js.BAK" ] && mv "$PPATH/index.js" "$PPATH/index.js.BAK"
  cp "$MODULE/bitcoreLoader.js" "$PPATH/index.js"
}
PPATH="bitcore-lib-zcash" ; replaceBitcoreLoader

cd "$WHEREAMI"
