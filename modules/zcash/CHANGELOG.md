2024.03.07
- started changelog per coin module

2024.03.28
- created minimal library based on bitcore for Zcash
- bitcore-lib-zcash in npm is buggy, so this separate version has been bugfixed!
- keep it separate from other node modules!
- signed txs are not accepted on the network: mandatory-script-verify-flag-failed
  - may have to do with some kind of version numbering


