const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  entry: './deterministic.js',
  output: {
    path: path.resolve(__dirname, '.'),
    filename: 'bundle.js',
    library: 'test' // added to create a library file
  },
  resolve: {
    fallback: {
      buffer: require.resolve('safe-buffer/'),
      stream: require.resolve('stream-browserify/'),
      url: require.resolve('url/'),
      crypto: require.resolve('crypto-browserify/'),
      vm: require.resolve('vm-browserify/')      
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
    }),
  ],
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
        extractComments: true,
        terserOptions: {
          mangle: {
            reserved: [
              'Buffer',
              'BigInteger',
              'Point',
              'ECPubKey',
              'ECKey',
              'sha512_asm',
              'asm',
              'ECPair',
              'HDNode'
            ]
          }
        }
      })
    ]
  }
};
