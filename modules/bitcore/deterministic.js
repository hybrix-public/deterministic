// (C) 2024 hybrix / Joachim de Koning
// hybrixd module - deterministic/bitcore
// Deterministic encryption wrapper for Bitcore compatible coins, like Bitcoin, Litecoin, Bitcoin Cash, etc.
//
// [!] Browserify this and save to deterministic.js.lzma to enable sending it from hybrixd to the browser!
//

if (!global.process) global.process = { version:'', env:{NODE_DEBUG:false} }; // hacky bugfix for webpack

const bitcore = {
  bitcoin:require('bitcore-lib'),
  bitcoincash:require('bitcore-lib-cash')
}

// remove globals
//delete global._litecore;
delete global._bitcore;
delete global._bitcoreCash;

// bitcoinjslib specific
const cashaddrjs = require('cashaddrjs');
const bchaddr = require('bchaddrjs');

/**
 * @param address
 * @param data
 */
const transformUtxo = data => unspent => ({
  address: data.source,
  outputIndex: unspent.txn,
  satoshis: Number(unspent.amount),
  script: unspent.script,
  txId: unspent.txid
});

/**
 * @param seed
 */
function mkPrivateKey (seed, mode) {
  const seedBuffer = Buffer.from(seed);
  const hash = nacl.to_hex(nacl.crypto_hash_sha256(seedBuffer));
  const bn = bitcore[mode].crypto.BN.fromBuffer(hash);
  return new bitcore[mode].PrivateKey(bn).toWIF();
}

/**
 * @param data
 */
function makeTransaction (data) {
  let toAddress =  data.target;
  let fromAddress = data.source;
  switch (data.mode) {
    case 'bitcoincash':
      toAddress = bchaddr.isLegacyAddress(data.target) ? bchaddr.toCashAddress(data.target) : data.target;
      fromAddress = bchaddr.isLegacyAddress(data.source) ? bchaddr.toCashAddress(data.source) : data.source;
      break;
  }

  const privKey = bitcore[data.mode].PrivateKey(data.keys.WIF);
  const hasValidMessage = typeof data.msg !== 'undefined' && data.msg !== null && data !== '';
  const amount = Number(data.amount);
  const fee = Number(data.fee);
  const utxos = data.unspent.unspents.map(transformUtxo(data));

  const transaction = new bitcore[data.mode].Transaction()
    .from(utxos)
    .change(fromAddress)
    .fee(Number(fee))
    .to(toAddress, Number(amount));

  const transactionWithMsgOrDefault = hasValidMessage
    ? transaction.addData(data.msg)
    : transaction;

  const signedTransaction = transactionWithMsgOrDefault
    .sign(privKey)
    .uncheckedSerialize();
    //serialize();

  return signedTransaction;
}

/**
 * @param WIF
 */
function mkPublicKey (WIF, mode) {
  // reference: https://learnmeabitcoin.com/technical/public-key
  const publicKey = bitcore[mode].PublicKey(bitcore[mode].PrivateKey(WIF));
  return publicKey.toString();
}

/**
 * @param WIF
 * @param mode
 */
function mkAddressLegacy (WIF, mode) {
  let address;
  switch (mode) {
    case 'bitcoincash':
      address = bchaddr.toLegacyAddress(mkAddress(WIF, mode));
      break;
    default:
      address = mkAddress(WIF, mode);
  }
  return address;
}

/**
 * @param WIF
 * @param mode
 */
function mkAddress (WIF, mode) {
  const address = bitcore[mode].PrivateKey(WIF).toAddress();
  const type = address.type === bitcore[mode].Address.PayToPublicKeyHash ? 'P2PKH' : 'P2SH';
  const hash = new Uint8Array(address.hashBuffer);
  let result
  switch (mode) {
    case 'bitcoincash':
      result = cashaddrjs.encode('bitcoincash', type, hash);  //  mode = ['bitcoincash', 'bchtest', 'bchreg'];
      break;
    default:
      result = address.toString();
  }  
  return result;
}

const wrapper = {
  // create deterministic public and private keys based on a seed
  keys: data => {
    const WIF = mkPrivateKey(data.seed, data.mode);
    return {
      WIF: WIF,
      publicKey: mkPublicKey(WIF, data.mode),
      addressLegacy: mkAddressLegacy(WIF, data.mode),
      address: mkAddress(WIF, data.mode)
    };
  },

  // import private key in WIF-format
  importPrivate: data => ({
    WIF: data.privateKey,
    publicKey: mkPublicKey(data.privateKey, data.mode),
    addressLegacy: mkAddressLegacy(data.privateKey, data.mode),
    address: mkAddress(data.privateKey, data.mode)
  }),

  // return private key
  privatekey: data => data.WIF,

  // return public key
  publickey: data => data.publicKey,

  // generate a unique wallet address from a given public key
  address: data => data.address,

  // return deterministic transaction data
  transaction: data => makeTransaction(data)
};

window.deterministic = wrapper;
