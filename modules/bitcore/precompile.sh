#!/bin/sh
OLDPATH=$PATH
WHEREAMI=`pwd`

# $HYBRIXD/deterministic/modules/bitcore  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
DETERMINISTIC="$HYBRIXD/deterministic"
MODULE="$DETERMINISTIC/modules/bitcore"

# remove process.env.NODE
removeProcessGlobal () {
  sed 's/process.env.NODE_DEBUG/false/g' "$MODULE/$PFILE" > "$MODULE/$PFILE.tmp"
  mv "$MODULE/$PFILE.tmp" "$MODULE/$PFILE"
}

# Replace all bitcore loaders with minimalist versions
replaceBitcoreLoader () {
  [ ! -f "$PPATH/index.js.BAK" ] && mv "$PPATH/index.js" "$PPATH/index.js.BAK"
  cp "$MODULE/bitcoreLoader.js" "$PPATH/index.js"
}
PPATH="node_modules/bitcore-lib" ; replaceBitcoreLoader
PPATH="node_modules/bitcore-lib-cash" ; replaceBitcoreLoader
PPATH="node_modules/bitcore-lib-zcash" ; replaceBitcoreLoader


# Fix error caused by fault in deserialization of buffer to transaction
sed 's/var copy = new Transaction(transaction.toBuffer());/var copy = new Transaction(transaction.toObject());/g' "$MODULE/node_modules/bitcore-lib-cash/lib/transaction/transaction.js" > "$MODULE/transaction.js.tmp"
mv "$MODULE/transaction.js.tmp" "$MODULE/node_modules/bitcore-lib-cash/lib/transaction/transaction.js"
rm -f  "$MODULE/transaction.js.tmp"

# Fix toBuffer issue that comes up with webpack
cp "$MODULE/bn_replacement.js" "$MODULE/node_modules/elliptic/node_modules/bn.js/lib/bn.js"

# Usage of process is not allowed in deterministic code! 
PFILE="node_modules/util/util.js" ; removeProcessGlobal
PFILE="node_modules/bitcore-lib/bitcore-lib.js" ; removeProcessGlobal
PFILE="node_modules/core-util-is/float.patch" ; removeProcessGlobal

cd "$WHEREAMI"
