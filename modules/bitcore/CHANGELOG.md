2024.03.07
- started changelog per coin module

2024.03.28
- updated bitcore-lib and bitcore-lib-cash to webpack v5 and terser
- note that it is sometimes better to keep separate legacy or badly supported codebases like Zcash!


