#!/bin/sh
OLDPATH=$PATH
WHEREAMI=`pwd`

# $HYBRIXD/deterministic/modules/ethereum  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
DETERMINISTIC="$HYBRIXD/deterministic"
MODULE="$DETERMINISTIC/modules/ethereum"

# remove process.env.NODE
removeProcessGlobal () {
  sed 's/process.env.NODE_DEBUG/false/g' "$MODULE/$PFILE" > "$MODULE/$PFILE.tmp"
  mv "$MODULE/$PFILE.tmp" "$MODULE/$PFILE"
}

# Usage of process is not allowed in deterministic code! 
PFILE="node_modules/util/util.js" ; removeProcessGlobal

cd "$WHEREAMI"
