const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  entry: './deterministic.js',
  output: {
    path: path.resolve(__dirname, '.'),
    filename: 'bundle.js',
    library: 'test' // added to create a library file
  },
  resolve: {
    fallback: {
      buffer: require.resolve('safe-buffer/'),
      stream: require.resolve('stream-browserify/'),
      assert: require.resolve('assert/')
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer']
    })
  ],
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
        extractComments: true
      })
    ]
  }
};
