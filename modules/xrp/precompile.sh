#!/bin/sh
OLDPATH=$PATH
WHEREAMI=`pwd`

# $HYBRIXD/deterministic/modules/xrp  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
DETERMINISTIC="$HYBRIXD/deterministic"
MODULE="$DETERMINISTIC/modules/xrp"

# Usage of process is not allowed in deterministic code!
sed 's_var asyncWrite = !process.browser_var asyncWrite = false; // DEPRECATED: var asyncWrite = !process.browser_g' "$MODULE/node_modules/readable-stream/lib/_stream_writable.js" > "$MODULE/_stream_writable.js.tmp"
sed 's/process.env.NODE_DEBUG/false/g' "$MODULE/node_modules/util/util.js" > "$MODULE/util.js.tmp"
mv "$MODULE/_stream_writable.js.tmp" "$MODULE/node_modules/readable-stream/lib/_stream_writable.js"
mv "$MODULE/util.js.tmp" "$MODULE/node_modules/util/util.js"

cd "$WHEREAMI"
