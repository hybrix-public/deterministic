// hybrixd module - electrum/deterministic_source.js
// Deterministic encryption wrapper for Bitcoin
//
// [!] Browserify this and save to deterministic.js.lzma to enable sending it from hybrixd to the browser!
//

if (!global.process) global.process = { version:'', env:{NODE_DEBUG:false} }; // hacky bugfix for webpack

const bitcoinjslib = require('./bitcoinjs-lib-smly/src/index.js');
const BigInteger = require('bigi')
const network = 'smileycoin';

/**
 * @param seed
 * @param network
 */
function mkKeyPair (seed, network) {
  const hash = bitcoinjslib.crypto.sha256(seed);
  const d = BigInteger.fromBuffer(hash);  
  const options = {
    compressed: false,
    network: bitcoinjslib.networks[network]
  };
  const keyPair = new bitcoinjslib.ECPair(d, undefined, options)
  return keyPair;
}

/**
 * @param WIF
 * @param network
 */
function mkKeyPairFromWIF (WIF, network) {
  return bitcoinjslib.ECPair.fromWIF(WIF, bitcoinjslib.networks[network]);
}

/**
 * @param keyPair
 */
function mkPublicKey (keyPair) {
  // reference: https://learnmeabitcoin.com/technical/public-key
  return keyPair.getPublicKeyBuffer().toString('hex');
}

/**
 * @param keyPair
 * @param network
 */
function mkAddress (keyPair, network) {
  return keyPair.getAddress();
}

const wrapper = {

  // create deterministic public and private keys based on a seed
  keys: data => {
    const keyPair = mkKeyPair(data.seed, network);
    const WIF = keyPair.toWIF();
    const publicKey = mkPublicKey(keyPair);
    const address = mkAddress(keyPair, network);
    return {
      WIF,
      publicKey,
      address
    };
  },

  importPrivate: function (data) {
    const keyPair = mkKeyPairFromWIF(data.privateKey, network);
    const publicKey = mkPublicKey(keyPair);
    const address = mkAddress(keyPair, network);
    return {
      WIF: data.privateKey,
      publicKey,
      address
    };
  },

  // generate a unique wallet address from a given public key
  address: data => data.address,

  // return public key
  publickey: data => data.publicKey,

  // return private key
  privatekey: data => data.WIF,

  // return deterministic transaction data
  transaction: data => {
    const keyPair = bitcoinjslib.ECPair.fromWIF(data.keys.WIF, bitcoinjslib.networks[network]);
    const tx = new bitcoinjslib.TransactionBuilder(bitcoinjslib.networks[network]);

    // add inputs
    data.unspent.unspents.forEach(function (unspent, i) {
      tx.addInput(unspent.txid, parseInt(unspent.txn));
    });

    let target;
    if (data.target.startsWith('bc1')) {
      throw new Error(`Currently hybrix does not support Segwit transactions!`);
    } else {
      target = data.target;
    }
    // add spend amount output
    tx.addOutput(target, parseInt(data.amount));

    // send back change
    const outchange = parseInt(data.unspent.change); // fee is already being deducted when calculating unspents
    if (outchange > 0) tx.addOutput(data.source, outchange);


    if (data.mode === 'bitcoincash' || data.mode === 'smileycoin') { // sign inputs SIGHASH
      var hashType = bitcoinjslib.Transaction.SIGHASH_ALL | bitcoinjslib.Transaction.SIGHASH_BITCOINCASHBIP143;
      // NOT WORKING: tx.enableBitcoinCash(true);
      data.unspent.unspents.forEach(function (unspent, i) {
        tx.sign(i, keyPair, null, hashType, unspent.value);
      });
    } else { // sign inputs normally
      data.unspent.unspents.forEach(function (unspent, i) {
        tx.sign(parseInt(i), keyPair);
      });
      tx[0] = tx.build();
    }
    
    return tx.build().toHex();
  }
};

// export the functionality to a pre-prepared var
window.deterministic = wrapper;
