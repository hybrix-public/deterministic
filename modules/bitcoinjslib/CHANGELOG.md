2024.03.07
- started changelog per coin module
- updated bitcoinjslib and dependencies to webpack v5 and terser
- added Smilycoin (SMLY) coininfo

2024.03.28
- deprecated Smileycoin, Zcash and Bitcoin cash from coininfo
