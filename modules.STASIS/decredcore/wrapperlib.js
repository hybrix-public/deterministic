//
// wrapperlib to include libraries for incorporation into the virtual DOM
//


// inclusion of necessary requires
var decredcorelib = {
  decredcore : require('./dcrcore-lib/index.js')
}

module.exports = decredcorelib;
