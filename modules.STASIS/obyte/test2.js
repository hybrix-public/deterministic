const bitcore = require('bitcore-lib');
const obyte = require('obyte');
const { toWif, getChash160 } = require('obyte/lib/utils');

const testnet = false;
const path = testnet ? "m/44'/1'/0'/0/0" : "m/44'/0'/0'/0/0";

const seedString = 'some rand#$$@%z01%d data string 3498234somedata string 3498234somedata string 3498234somedata string 3498234somedata string 3498234some random Seed data string 3498234some rand#$$@%z01%d data string 3498234some random Seed data string 3498234';

/*
let mnemonic = new Mnemonic(Buffer.from(seedString.substr(0,192)),Mnemonic.Words.ENGLISH);
console.log('mnemonic: '+mnemonic.toString());
*/


const xPrivKey = bitcore.HDPrivateKey.fromSeed( Buffer.from(seedString.substr(0,64)));
const { privateKey } = xPrivKey.derive(path);
const privKeyBuf = privateKey.bn.toBuffer({ size: 32 });
const wif = toWif(privKeyBuf, testnet);
const pubkey = privateKey.publicKey.toBuffer().toString('base64');
const definition = ['sig', { pubkey }];
const address = getChash160(definition);

console.log(
  'Seed:', seedString.substr(0,64),
  '\nPath:', path,
  '\nWIF:', wif,
  '\nPublic key:', pubkey,
  '\nAddress:', address
);


// RAW TRANSACTION
payload = {
            inputs: [
              {
                type: 'witnessing',
                from_main_chain_index: 580671,
                to_main_chain_index: 580684
              }
            ],
            outputs: [
              {
                address: 'S7N5FE42F6ONPNDQLCF64E2MGFYKQR2I',
                amount: 79
              }
            ]
          }

const client = new obyte.ClientCUSTOM();
//const client = new obyte.ClientCUSTOM(null, {testnet: false});
(async () => {
    const r = await client.compose.message('payment', payload, wif);
    /*
    const r = await client.compose.message('payment',
            {
                inputs: ['1','2','3'],
                outputs: [
                    { address: address, amount: 1000 }
                ]
            },
            wif);
    */
    console.log(r);
})();
