// (C) 2015-present hybrix / Joachim de Koning
// hybrixd module - tron/deterministic.js
// Deterministic encryption wrapper for Tron
const Decimal = require('../../common/crypto/decimal');
Decimal.set({ precision: 64 });

const hex2base32 = require('./../../common/crypto/hex2base32.js');

const CryptoUtils = require('@tronscan/client/src/utils/crypto');
const TransactionUtils = require('@tronscan/client/src/utils/transactionBuilder');

const publicKeyOrAddress = function(privateKey) {
  const buffer = Buffer.from(privateKey, 'hex');
  const privKeyBase64 = buffer.toString('base64');
  return CryptoUtils.getBase58CheckAddressFromPriKeyBase64String(privKeyBase64);
}

const deterministic = {
  // create deterministic public and private keys based on a seed (must be 64 chars in length!)
  keys: data => {
    const privKey = hex2base32.base32ToHex(data.seed).substr(0, 64).toLowerCase();
    return { privateKey: privKey };
  },
  
  importPrivate: function (data) {
    return {privateKey: data.privateKey};
  },

  // generate a unique wallet address from a given public key
  address: function (data) {
    return publicKeyOrAddress(data.privateKey);
  },

  // return public key
  publickey: function (data) {
    return publicKeyOrAddress(data.privateKey);
  },

  // return private key
  privatekey: function (data) {
    return data.privateKey;
  },

  // generate a transaction (TRC10)
  // now need to use Tronweb, but we need to somehow make sure it does not phone home!
  // check out: https://github.com/tronprotocol/tronweb/blob/master/test/lib/transactionBuilder.test.js
  transaction: function (data, dataCallback, errorCallback) {
    if (!data.hasOwnProperty('contract')) {
      errorCallback('Missing contract (pre-transactional) data!');
      return;
    }
    const privateKey = data.keys.privateKey;
    const target = data.target;
    // const address = data.source;
    const source = CryptoUtils.pkToAddress(privateKey);
    const amount = Number(data.amount);
    const token = data.contract?data.contract.toUpperCase():'0';
    const transaction = TransactionUtils.buildTransferTransaction(token, source, target, amount);
    const signedTransaction = CryptoUtils.signTransaction(privateKey, transaction);
    const rawTx = (signedTransaction && signedTransaction.hex)?signedTransaction.hex:null;
    dataCallback(rawTx);
  }

};

// export functionality to a pre-prepared var
window.deterministic = deterministic;
