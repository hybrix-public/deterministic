// (C) 2024 hybrix / Joachim de Koning
// hybrixd module - deterministic/spawnDeterministic.js
// Utility that spawns separate a process to activate a deterministic code blob

const fs = require('fs');
const util = require('util');
const fsExists = util.promisify(fs.exists);
const fsReadFile = util.promisify(fs.readFile);
const process = require('process');

const LZString = require('../../common/crypto/lz-string');
const { activate } = require('../../common/index');

const keysFile = '../hybrixd.keys';
const DEFAULT_SALT = 'F4E5D5C0B3A4FC83F4E5D5C0B3A4AC83F4E5D000B9A4FC83'; // TODO define in conf?

const nacl_factory = require('../../common/crypto/nacl.js');
nacl_factory.instantiate (async naclinstance => {
  nacl = naclinstance; // nacl is a global that needs to be initialized here
  let keys;

  const success = (result) => {
    process.send({err: 0, result});
  }

  const failure = (result) => {
    const errorMessage = `spawnDeterministic -> ${result}`;
    process.send({err: 1, errorMessage});
  }

  // decrypt the input data
  if (fs.existsSync(keysFile)) {
    try {
      keys = JSON.parse(fs.readFileSync(keysFile));
    } catch (err) {
      failure(`Failed to read key file ${keysFile}!`);
    }
  }

  const loadBlob = async function (filePath, dataCallback, errorCallback) {
    if (await fsExists(filePath)) {
      const buffer = await fsReadFile(filePath);
      return dataCallback(buffer.toString());
    } else {
      return errorCallback(`Cannot find ${filePath}!`);
    }
  }

  // parse and decrypt input data, then take action synchronously using a promise construction
  const getMessage = new Promise((resolve, reject) => { process.once('message', (message) => { resolve(message); }); });
  const encryptedMessage = await getMessage;

  try {
    const user_keys = {boxPk: nacl.from_hex(keys.publicKey), boxSk: nacl.from_hex(keys.secretKey.substr(0, 64))}; // cut off pubkey for boxSk!
    const nonce_salt = nacl.from_hex(DEFAULT_SALT);
    const crypt_hex = nacl.from_hex( decodeURIComponent(encryptedMessage) );
    const crypt_bin = nacl.crypto_box_open(crypt_hex, nonce_salt, user_keys.boxPk, user_keys.boxSk); // use nacl to create a crypto box containing the data
    inputData = JSON.parse( nacl.decode_utf8(crypt_bin) );
  } catch (err) {
    failure(`Failed to decrypt deterministic input data! -> ${err.message}`);
  }

  /*  Example:
   *  const inputData = {
   *    id:"dummycoin",
   *    method:"keys",
   *    data:{}
   *  }
   */
  const filePath = `../modules/deterministic/${inputData.id}/deterministic.js.lzma`;

  //
  // 1. prepare the environment for loading the blob (like in interface)
  //

  // emulate window for browser code executed in nodejs environment
  if (typeof window === 'undefined') window = {};
  if (typeof window.crypto === 'undefined') window.crypto = require('crypto');

  // In browser implementations a window.crypto.getRandomValues is expected
  // this is not in nodjes crypto library so we define it here in case
  // we want to use browser code in a nodejes environment
  if (typeof window.crypto.getRandomValues === 'undefined') {
    window.crypto.getRandomValues = function getRandomValues (arr) {
      const bytes = window.crypto.randomBytes(arr.length);
      for (let i = 0; i < bytes.length; i++) arr[i] = bytes[i];
    };
  }
  // Likewise in a nodejs implementation crypto.randomBytes is expected
  // this is not available in a browser envrioment so we define it here in case
  // we want to use nodejs code in a browser environment
  if (typeof window.crypto.randomBytes === 'undefined') {
    window.crypto.randomBytes = function (size, callback) {
      const bytes = [];
      for (let i = 0; i < bytes.length; i++) bytes.push(0);
      window.crypto.getRandomValues(bytes); // overwrite the zero values with random values
      if (typeof callback === 'function') callback(null, bytes);
      else return bytes;
    };
  }

  if (typeof crypto === 'undefined') crypto = window.crypto; // Needed to make ethereum work
  if (typeof crypto.getRandomValues === 'undefined') crypto.getRandomValues = window.crypto.getRandomValues;

  // emulate self for browser code executed in nodejs environment
  if (typeof self === 'undefined') self = {};
  if (typeof self.crypto === 'undefined') self.crypto = window.crypto;
  if (typeof FormData === 'undefined') FormData = {};

  //
  // 2. load and activate the blob
  //
  
  const loadAndActivateBlob = (filePath) => {
    loadBlob(filePath, (blob) =>
      {
        try {
          const code = LZString.decompressFromEncodedURIComponent(blob);
          const deterministicModule = activate(code);
          if (deterministicModule) { // we activated the blob! now perform the actions
            const syncResult = deterministicModule[inputData.method](inputData.data,success,failure)
            if (typeof syncResult !== 'undefined') {
              success(syncResult,true);
            }
          } else failure('Failed to activate deterministic code!');
        } catch (err) {
          failure(`Deterministic code runtime error: ${err}`);
          console.trace(e);
        }
      },
      (e) => failure(e)
    );
  }
  window.nacl = nacl;
  loadAndActivateBlob(filePath);  
});
